package com.sky.assignment

import akka.actor.ActorSystem
import spray.routing.{Route, SimpleRoutingApp}
import scala.util.{Failure, Success}
import spray.http.StatusCodes._

/**
 * Spray based App which delegates recommendation requests to the RecsEngine Webservice and
 * Transforms and Combines XML responses into JSON.
 */
object Application extends App with SimpleRoutingApp {

  implicit val system = ActorSystem("recs")
  import scala.concurrent.ExecutionContext.Implicits.global


  startServer(interface = "localhost", port = 8090) {
    routes
  }

  def routes: Route = {
    path("personalised" / Segment) { subscriber =>
      get {
        onComplete(SprayWebServiceCalls.callRecsEngineService(subscriber)) {
          case Success(recsApiResponse) => {
            import spray.json._
            import RecsAPIJsonProtocol._
            val jsonResponse = recsApiResponse.toJson
            complete {
                (OK, jsonResponse.prettyPrint)
            }
          }
          case Failure(error) =>
            complete(InternalServerError, s"An error occurred: ${error}")
        }
      }
    }
  }
}

