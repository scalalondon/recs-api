package com.sky.assignment

import akka.actor.ActorSystem
import spray.client.pipelining._
import spray.http.HttpHeaders.Accept
import spray.http.{HttpResponse, HttpRequest, MediaTypes}
import spray.httpx.encoding.{Deflate, Gzip}

import scala.concurrent.Future
import scala.util.{Failure, Success}
import scala.util.control.NonFatal

/**
 * Implement External WebService Calls.
 */
object SprayWebServiceCalls extends WebServiceCalls {

  import scala.concurrent.ExecutionContext.Implicits.global

  implicit val system = ActorSystem("recs")
  val resAPIConfig = RecsAPIConfig(system)


  type Recovery[T] = PartialFunction[Throwable,T]

  // recover with empty sequence
  def withEmptySeq[T]:Recovery[Seq[T]] = { case NonFatal(e) => Seq() }

  def callRecsEngineService(subscriber: String): Future[RecsApiResponse[RecsApiHourlyResponse[Recommendation]]] = {

    import spray.httpx.unmarshalling._

    import RecsEngineXmlUnmarshaller._

    val endPoint = s"http://${resAPIConfig.Server.name}:${resAPIConfig.Server.port}/recs/personalised?num=10&start=10&end=10&subscriber=acs"
    val acceptHeader = Accept(MediaTypes.`application/xhtml+xml`,MediaTypes.`application/xml`,MediaTypes.`text/xml` )

    val pipeline : HttpRequest => Future[Seq[Recommendation]] = (
      addHeader(acceptHeader)
        ~> encode(Gzip)
        ~> sendReceive
        ~> decode(Deflate)
        ~> unmarshal[Seq[Recommendation]])

    val hourlyTimeWindows = DateUtils.Hour.recommendationTimeWindows()

    val firstWindow = hourlyTimeWindows(0)
    val secondWindow = hourlyTimeWindows(1)
    val thirdWindow = hourlyTimeWindows(2)

    val firstHourFuture: Future[Seq[Recommendation]] =
      pipeline {
        Get(recsEngineEndpoint(RecsEngineRequest(resAPIConfig.Server.numberOfRecs, firstWindow._1, firstWindow._2, subscriber)))
      }

    val secondHourFuture: Future[Seq[Recommendation]] =
      pipeline {
        Get(recsEngineEndpoint(RecsEngineRequest(resAPIConfig.Server.numberOfRecs, secondWindow._1, secondWindow._2, subscriber)))
      }

    val thirdHourFuture: Future[Seq[Recommendation]] =
      pipeline {
        Get(recsEngineEndpoint(RecsEngineRequest(resAPIConfig.Server.numberOfRecs, thirdWindow._1, thirdWindow._2, subscriber)))
      }

    for {
      firstHourRecommendations <- firstHourFuture
      secondHourRecommendations <- secondHourFuture
      thirdHourRecommendations <- thirdHourFuture
    } yield {
      val firstHourResponse = RecsApiHourlyResponse(firstHourRecommendations, firstWindow._2)
      val secondHourResponse = RecsApiHourlyResponse(secondHourRecommendations, secondWindow._2)
      val thirdHourResponse = RecsApiHourlyResponse(firstHourRecommendations, thirdWindow._2)
      RecsApiResponse(Seq(firstHourResponse, secondHourResponse, thirdHourResponse))
    }

  }

  /**
   * Build the URL to RecsEngine Service
   * @param r
   * @return
   */
  def recsEngineEndpoint(r: RecsEngineRequest): String = {
    s"http://${resAPIConfig.Server.name}:${resAPIConfig.Server.port}/recs/personalised?num=${r.num}&start=${r.start}&end=${r.end}&subscriber=${r.subscriber}"
  }

}
