package com.sky.assignment

import com.typesafe.config.Config
import akka.actor.{ExtensionIdProvider, ExtensionId, Extension, ExtendedActorSystem}

/**
 * Allow modification of Service Endpoints etc.
 */
class RecsAPIConfig(config: Config, extendedSystem: ExtendedActorSystem) extends Extension {

  object Server {
    val name = config.getString("recs-engine-service.server.name")
    val port = config.getInt("recs-engine-service.server.port")
    val numberOfRecs = config.getInt("recs-engine-service.numberOfRecs")
  }
}

object RecsAPIConfig extends ExtensionId[RecsAPIConfig] with ExtensionIdProvider {

  override def lookup = RecsAPIConfig

  override def createExtension(system: ExtendedActorSystem) =
    new RecsAPIConfig(system.settings.config, system)

}

