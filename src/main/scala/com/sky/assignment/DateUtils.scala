package com.sky.assignment

/**
 * Object for working with Dates.
 */
import java.util.Date

object DateUtils {

  object Hour {

    val milliSecondsInAnHour = (60 * 60 * 1000);

    /**
     * Return tuple of start of hour and end of hour.
     *
     * @return tuple(startOfHourMillis,endOfHourMills)
     */
    def currentWindow: (Long,Long) = {
      val now = (new Date()).getTime
      val start = now - (now % milliSecondsInAnHour)
      val end = start + milliSecondsInAnHour
      (start, end)
    }

    /**
     * Next 3 Hour Windows.
     *
     * @return
     */
    def recommendationTimeWindows(): List[(Long,Long)]  = {
      val firstWindow = currentWindow
      val secondWindow = (firstWindow._1 + milliSecondsInAnHour, firstWindow._2 + milliSecondsInAnHour)
      val thirdWindow = (secondWindow._1 + milliSecondsInAnHour, secondWindow._2 + milliSecondsInAnHour)
      List(firstWindow, secondWindow, thirdWindow)
    }

  }

}