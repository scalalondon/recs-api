package com.sky.assignment

import scala.concurrent.Future

/**
 * Service which need to be implemented (both actual http webservice requests and allow for mocking of this service).
 */
trait WebServiceCalls {

  /**
   * Call RecsEngine remote webservice.
   *
   * @param subscriber
   * @return
   */
  def callRecsEngineService(subscriber: String): Future[RecsApiResponse[RecsApiHourlyResponse[Recommendation]]]

}