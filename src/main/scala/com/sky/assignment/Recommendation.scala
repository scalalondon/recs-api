package com.sky.assignment

import spray.json.{JsonFormat, DefaultJsonProtocol}

/**
 * Implement Data Transfer Objects and Transformations.
 */

/**
 * Represent item in XML response from Remote RecsEngine Service.
 *
 * @param uuid
 * @param start
 * @param end
 */
case class Recommendation(uuid: String, start: Long, end: Long)

/**
 * Remote Service RecsEngine response Data Structure.
 *
 * @param results
 */
case class RecsEngineResponse[T](results: Seq[T])

/**
 * RecsAPI WebService Response Data structure.
 *
 * @param recsApiHourlyResponse
 */
case class RecsApiResponse[T](recsApiHourlyResponse: Seq[T])

/**
 * Build final response returned to the user. Aggregate RecsApiHourlyResponse
 *
 * @param recommendations
 * @param expiry
 * @tparam T
 */
case class RecsApiHourlyResponse[T](recommendations: Seq[T], expiry: Long)

/**
 * Compose Request to RecsEngine.
 * <p> Example call:
 * http://localhost:8080/recs/personalised?num=10&start=1424509958828&end=1424513438828&subscriber=acs
 */
case class RecsEngineRequest(num: Integer, start: Long, end: Long, subscriber: String)

/**
 * JSON Marsall/UnMarshall implementations
 */
object RecsAPIJsonProtocol extends DefaultJsonProtocol {
  implicit val recommendationFormat = jsonFormat3(Recommendation)
  implicit def recsApiHourlyResponseFormat[T :JsonFormat] = jsonFormat2(RecsApiHourlyResponse.apply[T])
  implicit def recsApiResponseFormat[T :JsonFormat] = jsonFormat1(RecsApiResponse.apply[T])

}

/**
 * An Object which contains the necessary code to parse XML response from RecsEngine.
 *
 * Reference code: http://spray.io/documentation/1.2.2/spray-httpx/unmarshalling/#custom-unmarshallers
 */
object RecsEngineXmlUnmarshaller {

  import java.io.{ByteArrayInputStream, InputStreamReader}
  import scala.xml.XML
  import spray.httpx.unmarshalling._
  import spray.http._
  import MediaTypes._

  implicit val RecommendationUnmarshaller
  = Unmarshaller[Seq[Recommendation]](`text/xml`, `application/xml`, `application/xhtml+xml`) {
    case HttpEntity.NonEmpty(contentType, data) ⇒
      val parser = XML.parser
      try {
        parser.setProperty("http://apache.org/xml/properties/locale", java.util.Locale.ROOT)
      } catch {
        case e: org.xml.sax.SAXNotRecognizedException ⇒ // property is not needed
      }
      val rootElement = XML.withSAXParser(parser).load(new InputStreamReader(new ByteArrayInputStream(data.toByteArray), contentType.charset.nioCharset))
      val recommendations = (rootElement \ "recommendations").map { rec =>
        Recommendation((rec \ "uuid").text, (rec \ "start").text.toLong, (rec \ "end").text.toLong)
      }
      recommendations
  }


}