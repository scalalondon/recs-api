package com.sky.assignment

import org.scalatest.MustMatchers
import org.scalatest.WordSpec
import scala.concurrent.Await
import scala.concurrent.duration._

/**
 * Test RecommendationServiceSpec.
 */
class SprayWebServiceCallsTestSpec extends WordSpec with MustMatchers {

  "SprayWebServiceCallsTestSpec" must {
    "return recommendations" in {

      val response = Await.result(SprayWebServiceCalls.callRecsEngineService(subscriber = "asd"), 10.seconds)

      response.recsApiHourlyResponse.size mustEqual(3)

      val firstHour = response.recsApiHourlyResponse(0)
      firstHour.recommendations.size mustEqual(5)

      val secondHour = response.recsApiHourlyResponse(1)
      secondHour.recommendations.size mustEqual(5)

      val thirdHour = response.recsApiHourlyResponse(2)
      thirdHour.recommendations.size mustEqual(5)


    }
  }
}
