package com.sky.assignment

import scala.concurrent.Future

/**
 * An implementation of WebServiceCalls for Test purposes.
 */
trait MockWebServiceCalls extends WebServiceCalls {

  // Test Fixtures
  val recommendations =  Seq(Recommendation("rec1", 0, 30),
    Recommendation("rec2", 0, 60),
    Recommendation("rec3", 0, 90))

  import scala.concurrent.ExecutionContext.Implicits.global

  def callRecsEngineService(subscriber: String): Future[RecsApiResponse[RecsApiHourlyResponse[Recommendation]]] = {
    Future {
      RecsApiResponse(Seq(RecsApiHourlyResponse(recommendations,10)))
    }
  }

}