package com.sky.assignment

import org.scalatest._
import spray.routing.HttpService
import spray.http.StatusCodes._
import spray.testkit.ScalatestRouteTest

/**
 * Test Interactions with the RecsEngine.
 */
class ApplicationTestSpec extends WordSpec with ScalatestRouteTest with Matchers with HttpService {

  def actorRefFactory = system

  "The Recommendations Service" must  {
    "when calling GET personalised/subscriber should return recommendations" in {
        Get("/personalised/subscriber") ~> Application.routes ~> check {
          status should equal(OK)
          entity.toString should include("recommendations")
        }
      }
    }

}
