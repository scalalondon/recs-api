package com.sky.assignment


import org.scalatest.{MustMatchers, WordSpec}

/**
 * Test Marshalling and Unmarshalling of XML and JSON.
 */
class RecommendationTestSpec extends WordSpec with MustMatchers {

  val recommendation1 = Recommendation("rec-1", 0,  100)
  val recommendation2 = Recommendation("rec-2", 50, 150)


  "RecsAPIJsonProtocol" must {
    "Marshall and Unmarshall Recommendation to and from JSON format" in {
      import spray.json._
      import RecsAPIJsonProtocol._
      val json = recommendation1.toJson
      json.convertTo[Recommendation] mustEqual(recommendation1)
      json.convertTo[Recommendation] must not equal(recommendation2)
    }
  }

  "RecsAPIJsonProtocol" must {
    "Marshall and Unmarshall RecsApiResponse to and from JSON format" in {
      val recommendations = Seq(recommendation1, recommendation2)
      val recsApiResponse = RecsApiHourlyResponse(recommendations,10);
      import spray.json._
      import RecsAPIJsonProtocol._
      val json = recsApiResponse.toJson
      json.convertTo[RecsApiHourlyResponse[Recommendation]] mustEqual(recsApiResponse)
    }
  }

  "RecsEngineXmlUnmarshaller" must {
    "be able to unmarshall XML and return a Seq[Recommendation]" in {

      import spray.httpx.unmarshalling._
      import spray.http._

      import RecsEngineXmlUnmarshaller._

      val body = HttpEntity(MediaTypes.`application/xml`, testFixtureRecommendationsXML.buildString(true) )
      val result = body.as[Seq[Recommendation]]

      result match {
        case Right(recommendations) => {
          recommendations must contain inOrderOnly(recommendation1, recommendation2)
        }
        case Left(error) => fail(s"oops, failure $error")
      }
    }
  }

  val testFixtureRecommendationsXML = {
    <recommendations>
      <recommendations>
        <uuid>{recommendation1.uuid}</uuid>
        <start>{recommendation1.start}</start>
        <end>{recommendation1.end}</end>
      </recommendations>
      <recommendations>
        <uuid>{recommendation2.uuid}</uuid>
        <start>{recommendation2.start}</start>
        <end>{recommendation2.end}</end>
      </recommendations>
    </recommendations>
  }


}


