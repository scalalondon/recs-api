package com.sky.assignment

import java.util.Date

import org.scalatest.{FlatSpec, ShouldMatchers}

/**
 * Test DataUtils
 */
class DateUtilTestSpec extends FlatSpec with ShouldMatchers {

  "DateUtils.Hour.currentWindow" should "get current start and end hourly time window" in {

    val currentTimeWindow = DateUtils.Hour.currentWindow
    currentTimeWindow._2 - currentTimeWindow._1 should equal(DateUtils.Hour.milliSecondsInAnHour)

    val now = (new Date()).getTime
    currentTimeWindow._1 shouldBe < (now)
    currentTimeWindow._2 shouldBe > (now)

    currentTimeWindow._1 shouldBe > (now - DateUtils.Hour.milliSecondsInAnHour+1)
    currentTimeWindow._2 shouldBe < (now + DateUtils.Hour.milliSecondsInAnHour+1 )

  }

  "DateUtils.Hour.recommendationTimeWindows" should "get relevant time windows for recommendations" in {

    val recommendationTimeWindows = DateUtils.Hour.recommendationTimeWindows()

    val firstWindow = recommendationTimeWindows(0)
    val secondWindow = recommendationTimeWindows(1)
    val thirdWindow = recommendationTimeWindows(2)

    firstWindow._1 shouldBe < (firstWindow._2)
    firstWindow._1 shouldBe < (secondWindow._1)
    firstWindow._2 shouldEqual secondWindow._1

    secondWindow._1 shouldBe < (secondWindow._2)
    secondWindow._1 shouldBe < (thirdWindow._1)
    secondWindow._2 shouldEqual thirdWindow._1

    thirdWindow._1 shouldBe < (thirdWindow._2)

    thirdWindow._2 shouldEqual(firstWindow._2 + 2 * DateUtils.Hour.milliSecondsInAnHour)
  }

}
